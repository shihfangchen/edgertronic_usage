# Edgertronic_usage

http://wiki.edgertronic.com/index.php/Main_Page

Access archieve video file : 
http://10.11.12.13/static/mnt/sdcard/DCIM/

Get the last saved video filename:  
`http://10.11.12.13/get_last_saved_filename`  

Trigger / Save / Download the latest video:  
```
curl http://10.11.12.13/trigger
curl http://10.11.12.13/save
curl -s http://10.11.12.13/get_last_saved_filename | sed -e 's/"//g' -e 's/^/http:\/\/10.11.12.13\/static/' > latest_vid_path
wget -i latest_vid_path
```
or  
> python3 trigger_save_download.py



